﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using contract;

namespace Conversion
{
    [Register(typeof(IConvert))]
    [ClassToMapAttribute]
    public class DataConvert : IConvert
    { 
        public T ConvertSingleDBsetToDTO<T>(DataSet data)
        {
            T temp = Activator.CreateInstance<T>();
            Type typeT = typeof(T);
            PropertyInfo[] properties = typeT.GetProperties();
            for (var col = 0; col < data.Tables[0].Columns.Count; col++)
            {
                properties.FirstOrDefault(p => p.Name == data.Tables[0].Columns[col].ColumnName)
                    .SetValue(temp, data.Tables[0].Rows[0][col]);
            }
            return temp;
        }
        public List<T> ConvertDBsetToDTO<T>(DataSet data)
        {
            List<T> listT = new List<T>();
            T temp = Activator.CreateInstance<T>();
            Type typeT = typeof(T);
            PropertyInfo[] properties = typeT.GetProperties();

            for (int row = 0; row < data.Tables[0].Rows.Count; row++)
            {
                for (var col = 0; col < data.Tables[0].Columns.Count; col++)
                {
                    properties.FirstOrDefault(p => p.Name == data.Tables[0].Columns[col].ColumnName)
                        .SetValue(temp, data.Tables[0].Rows[row][col]);
                }
                listT.Add(temp);
                temp = Activator.CreateInstance<T>();
            }
            return listT;
        }


        public List<SqlParameter> ConvertDTOToDBset<T>(T data)
        {
            Type typeT = typeof(T);
            PropertyInfo[] properties = typeT.GetProperties();
            List<SqlParameter> listParam = new List<SqlParameter>();
            foreach (var prop in properties)
            {
                if ((prop.GetCustomAttribute<DTO.IdentityAttribute>() == null ||
                (prop.GetCustomAttribute<DTO.IdentityAttribute>() != null && (int)prop.GetValue(data) != 0))
                && prop.GetCustomAttribute<DTO.NotIncludeAttribute>() == null)
                {
                    listParam.Add(new SqlParameter("@" + prop.Name, prop.GetValue(data)));
                }
            }
            return listParam;
        }

        public List<SqlParameter> ConvertSimpleTypeToDBset<T>(List<T> data)
        {
            List<SqlParameter> listParam = new List<SqlParameter>();
            data.ForEach(s =>
                listParam.Add(new SqlParameter("", s))
            );
            return listParam;
        }
        public List<SqlParameter> ConvertSingleSimpleTypeToDBset<T>(T data)
        {
            List<SqlParameter> listParam = new List<SqlParameter>();
            listParam.Add(new SqlParameter("", data));
            return listParam;
        }
    }
}

