﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    public class Resolver
    {

        static List<Assembly> DLL = new List<Assembly>();

        private static Dictionary<Type, Type> typeDict = new Dictionary<Type, Type>();
      public  static void LoadResolver()
        {
            DLL.Add( Assembly.LoadFile(@"D:\Users\User\Desktop\Dev++\NewCanvas\newserverside\DAL\bin\Debug\DAL.dll"));
            DLL.Add( Assembly.LoadFile(@"D:\Users\User\Desktop\Dev++\NewCanvas\newserverside\Conversion\bin\Debug\Conversion.dll"));
            DLL.ForEach(dll =>
            {
                var types = dll.GetTypes().ToList();
                types.ForEach(t =>
                {
                    if (t.GetCustomAttribute<contract.ClassToMapAttribute>() != null)
                    {
                        typeDict.Add(t.GetCustomAttribute<contract.RegisterAttribute>().Parent, t);
                    }
                });
            });
        }


        private static Resolver instance = null;
        public static Resolver GetInstance()
        {
            if (instance == null)
            {
                instance = new Resolver();
            }
            return instance;
        }

        public T Resolve<T>() where T : class
        {
            Type ty = typeDict[typeof(T)];
            T a = Activator.CreateInstance(ty) as T;
            return a;
        }
    }
}
