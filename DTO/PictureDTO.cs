﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PictureDTO
    {
        [Identity]
        public int pictureId { get; set; }
        // get, add,
        public int userId { get; set; }
        // get, add,
        public string pictureUrl { get; set; }
        // get,add
        public string pictureName { get; set; }
      [NotInclude]
        public int userTypeId { get; set; }
        // get,add
        public string descriptions { get; set; }
       
    }
}
