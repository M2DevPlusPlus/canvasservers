﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
  public  class ShapeDTO:DrawingDTO
    {
        [IdentityAttribute]
        public int shapeId { get; set; }
        public int shapeTypeId { get; set; }
        public float radiusX { get; set; }
        public float radiusY { get; set; }
        public float centerPointX { get; set; }
        public float centerPointY { get; set; }

    }
}
