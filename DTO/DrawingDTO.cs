﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class DrawingDTO
    {

        public int pictureId { get; set; }
        public string lineColor { get; set; }
        public string fillColor { get; set; }
        public string comment { get; set; }
        public int userId { get; set; }

        [NotInclude]
        public string userName { get; set; }



    }
}
