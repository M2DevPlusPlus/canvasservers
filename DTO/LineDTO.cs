﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class LineDTO:DrawingDTO
    {
        [IdentityAttribute]
        public int lineId { get; set; }
        public float startPointX { get; set; }
        public float startPointY { get; set; }
        public float endPointX { get; set; }
        public float endPointY { get; set; }


    }
}
