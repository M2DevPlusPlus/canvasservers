﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class ShareDTO
    {
        [Identity]
        public int shareId { get; set; }
        // get 
        public int pictureId { get; set; }
        [NotInclude]
        public string userName { get; set; }
        public int userId { get; set; }
        public int userTypeId { get; set; }

    }
}
