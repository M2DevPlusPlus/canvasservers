﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace contract
{
    public interface IPictureDAL
    {
        int AddNewPicture(List<SqlParameter> parameters);
        DataSet getPicturesPerUser(List<SqlParameter> parameters);
        int DeletePicture(List<SqlParameter> parameters);
        int UpdatePicture(List<SqlParameter> parameters);
    }
}
