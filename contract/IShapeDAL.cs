﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace contract
{
  public interface IShapeDAL
    {
        int addShape(List<SqlParameter> parameters);
        DataSet getShapesByPictureId(List<SqlParameter> parameters);
        int deleteShape(List<SqlParameter> parameters);
        int updateShape(List<SqlParameter> parameters);
    }
}
