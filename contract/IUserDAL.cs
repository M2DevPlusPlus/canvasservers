﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace contract
{
   public interface IUserDAL
    {
        int AddNewUser(List<SqlParameter> parameters);
        DataSet getUser(List<SqlParameter> parameters);
        int UpdateUser(List<SqlParameter> parameters);
        int DeleteUser(List<SqlParameter> parameters);
    }
}
