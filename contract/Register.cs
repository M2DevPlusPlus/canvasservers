﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace contract
{
    public class RegisterAttribute : Attribute
    {
        public Type Parent { get; set; }
        public RegisterAttribute()
        {

        }
        public RegisterAttribute(Type Parent)
        {
            this.Parent = Parent;
        }
    }

    
}
