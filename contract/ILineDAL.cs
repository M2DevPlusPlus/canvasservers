﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace contract
{
    public interface ILineDAL
    {
        DataSet getLinesByPictureId(List<SqlParameter> parameters);
        int DeleteLine(List<SqlParameter> parameters);
        int AddLine(List<SqlParameter> parameters);
        int UpdateLine(List<SqlParameter> parameters);

    }
}
