﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace contract
{
   public interface IShareDAL
    {
        int ShareNewUsers(List<SqlParameter> parameters);
        int DeleteShareUsers(List<SqlParameter> parameters);
        DataSet GetUsersPerPicture(List<SqlParameter> parameters);
    }
}
