﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace contract
{
   public interface IConvert
    {
        T ConvertSingleDBsetToDTO<T>(DataSet data);
        List<T> ConvertDBsetToDTO<T>(DataSet data);
        List<SqlParameter> ConvertDTOToDBset<T>(T data);
        List<SqlParameter> ConvertSimpleTypeToDBset<T>(List<T> data);
        List<SqlParameter> ConvertSingleSimpleTypeToDBset<T>(T data);
    }
}
