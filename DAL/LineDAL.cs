﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using contract;
namespace DAL
{
    [Register(typeof(ILineDAL))]
    [ClassToMapAttribute]
   public class LineDAL:ILineDAL
    {
        SuperDAL super = new SuperDAL();
        public  DataSet getLinesByPictureId(List<SqlParameter> parameters)
        {
           return super.get(parameters, "getLineByPictureId");
        }

        public int DeleteLine(List<SqlParameter> parameters)
        {
            return super.Delete(parameters, "deleteLine");
        }

        public int AddLine(List<SqlParameter> parameters)
        {
            return super.Add(parameters, "AddLine");
        }

        public int UpdateLine(List<SqlParameter> parameters)
        {
            return super.Update(parameters, "UpdateLine");
        }

    }
}
