﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using contract;
namespace DAL
{
    [Register(typeof(IShapeDAL))]
    [ClassToMapAttribute]
    public class ShapeDAL:IShapeDAL
    {
        SuperDAL super = new SuperDAL();
        //הוספת צורה
        public int addShape(List<SqlParameter> parameters)
        {
           return super.Add(parameters, "AddShape");


        }
        //שליפת כל הצורות של תמונה ע"י קוד תמונה
        public DataSet getShapesByPictureId(List<SqlParameter> parameters)
        {
           return super.get(parameters, "getShapesByPictureId");
        }
        //מחיקת צורה
        public int deleteShape(List<SqlParameter> parameters)
        {
            return super.Delete(parameters, "daleteShape");
        }
        //עדכון צורה
        public int updateShape(List<SqlParameter> parameters)
        {
            return super.Update(parameters, "UpdateShape");
        }


    }
}
