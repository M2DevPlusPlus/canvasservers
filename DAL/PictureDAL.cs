﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using contract;
namespace DAL
{
    [Register(typeof(IPictureDAL))]
    [ClassToMapAttribute]
    public class PictureDAL:IPictureDAL
    {
        SuperDAL super = new SuperDAL();
        public  int AddNewPicture(List<SqlParameter> parameters)
        {
           return super.Add(parameters, "AddPicture");
        }

        public DataSet getPicturesPerUser(List<SqlParameter> parameters)
        {
           return super.get(parameters, "GetPicturesPerUser");
        }

        public  int DeletePicture(List<SqlParameter> parameters)
        {
           return super.Delete(parameters, "deletePicture");
        }

        public int UpdatePicture(List<SqlParameter> parameters)
        {
            return super.Update(parameters, "UpdatePicture");
        }
    }
}
