﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace DAL
{
    public class DB
    {
        public SqlConnection DBcanvas;
        string serverName = "ONT";
        string databaseName = "Canvas";
        public void openDB()
        {
            DBcanvas = new SqlConnection();
            DBcanvas.ConnectionString = "Data Source=" + serverName + ";initial Catalog=" 
                + databaseName + ";Integrated Security=True";
            DBcanvas.Open();
        }
        public DB()
        {

        }
        public DB(string databaseName)
        {
            this.databaseName = databaseName;
        }
        public DB(string serverName,string databaseName):this(databaseName)
        {
            this.serverName = serverName;
        }
       
    }
}
