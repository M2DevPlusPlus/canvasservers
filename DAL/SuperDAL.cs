﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
   public class SuperDAL
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DataSet get(List<SqlParameter> parameters, string nameSP)
        {
            try
            {
                DB db= new DB();
                db.openDB();
                SqlCommand cmd = new SqlCommand(nameSP, db.DBcanvas);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < parameters.Count(); i++)
                {
                    cmd.Parameters[i + 1].Value = parameters[i].Value;
                }
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.DBcanvas.Close();
                log.Info("get data sucssesfully");
                return ds;
            }
            catch (Exception ex)
            {
                log.Fatal("get failed , exception is: " + ex.Message);
                //display error message
                return null;
            }
        }

        public int Delete(List<SqlParameter> parameters, string nameSP)
        {
            try
            {
                DB db;
                db = new DB();
                db.openDB();
                SqlCommand cmd = new SqlCommand(nameSP, db.DBcanvas);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < parameters.Count(); i++)
                {
                    cmd.Parameters[i + 1].Value = parameters[i].Value;
                }
                var temp = cmd.ExecuteNonQuery();
                db.DBcanvas.Close();
                log.Info("get data sucssesfully");

                return temp;
            }
            catch (Exception ex)
            {
                log.Fatal("get failed , exception is: " + ex.Message);

                //display error message
                return 0;
            }
        }


        public int Add(List<SqlParameter> parameters, string nameSP)
        {
            try
            {
                DB db;
                db = new DB();
                db.openDB();
                SqlCommand cmd = new SqlCommand(nameSP, db.DBcanvas);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddRange(parameters.ToArray());
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.DBcanvas.Close();
                log.Info("get data sucssesfully");

                return (int)ds.Tables[0].Rows[0][0];
            }
            catch (Exception ex)
            {
                log.Fatal("get failed , exception is: " + ex.Message);

                //display error message
                return 0;
            }
        }

        public int Update(List<SqlParameter> parameters, string nameSP)
        {
            try
            {
                DB db;
                db = new DB();
                db.openDB();
                SqlCommand cmd = new SqlCommand(nameSP, db.DBcanvas);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(parameters.ToArray());
                var temp = cmd.ExecuteNonQuery();
                db.DBcanvas.Close();
                log.Info("get data sucssesfully");
                return temp;
            }
            catch (Exception ex)
            {
                log.Fatal("get failed , exception is: " + ex.Message);

                //display error message
                return 0;
            }
        }
    }
}

