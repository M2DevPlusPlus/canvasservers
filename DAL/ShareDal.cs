﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using contract;
namespace DAL
{
    [Register(typeof(IShareDAL))]
    [ClassToMapAttribute]
    public class ShareDal:IShareDAL
    {
        SuperDAL super = new SuperDAL();
        public int ShareNewUsers(List<SqlParameter> parameters)
        {
           return super.Add(parameters, "ShareNewUsers");
        }
        public int DeleteShareUsers(List<SqlParameter> parameters)
        {
            return super.Delete(parameters, "DeleteShareUsers");
        }
        public DataSet GetUsersPerPicture(List<SqlParameter> parameters)
        {
            return super.get(parameters, "GetUsersPerPicture");
        }
    }
}
