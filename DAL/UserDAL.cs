﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using contract;
namespace DAL
{
    [Register(typeof(IUserDAL))]
    [ClassToMapAttribute]
    public class UserDAL:IUserDAL
    {
        SuperDAL super = new SuperDAL();

        public int AddNewUser(List<SqlParameter> parameters)
        {
           
              return super.Add(parameters, "addNewUser");          
        }
       
        public DataSet getUser(List<SqlParameter> parameters)
        {
            return super.get(parameters, "getUser");
        }
        public int UpdateUser(List<SqlParameter> parameters)
        {
            return super.Update(parameters, "updateUser"); 
        }

        public int DeleteUser(List<SqlParameter> parameters)
        {
            return super.Delete(parameters, "deleteUser");
        }
    }
}
